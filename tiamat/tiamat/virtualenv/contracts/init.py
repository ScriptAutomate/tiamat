from typing import List


def sig_bin(hub, bname: str) -> List[str]:
    ...


def sig_create(hub, bname: str):
    ...
