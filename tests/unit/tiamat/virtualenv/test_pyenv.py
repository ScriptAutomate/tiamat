from unittest import mock


def test_bin(hub, mock_hub, bname):
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].pyenv = bname
    mock_hub.tiamat.cmd.run.return_value = mock.MagicMock(retcode=0, stdout="TheRoots")
    mock_hub.tiamat.virtualenv.pyenv.bin = hub.tiamat.virtualenv.pyenv.bin
    mock_hub.tiamat.virtualenv.pyenv.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.pyenv.create = hub.tiamat.virtualenv.pyenv.create
    mock_hub.tiamat.virtualenv.pyenv.create(bname)
