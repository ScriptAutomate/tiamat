def test_run(hub):
    ret = hub.tiamat.cmd.run(["echo", "-n", "test"])
    assert not ret.retcode
    assert not ret.stderr
    assert ret.stdout == "test"
